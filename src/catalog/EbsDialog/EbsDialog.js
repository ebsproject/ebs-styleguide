import React from "react";
import PropTypes from "prop-types";
// CORE COMPONENTS
import {
  Dialog,
  DialogTitle,
  IconButton,
  Slide,
  Typography,
} from "@material-ui/core";
import { Close } from "@material-ui/icons";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const EbsDialogAtom = React.forwardRef(
  ({ open, handleClose, title, maxWidth, children }, ref) => {
    return (
      /* 
     @prop data-testid: Id to use inside EbsDialog.test.js file.
     */
      <div data-testid={"EbsDialogTestId"} ref={ref}>
        <Dialog
          open={open}
          TransitionComponent={Transition}
          onClose={handleClose}
          aria-labelledby="ebs-dialog-slide"
          aria-describedby="ebs-dialog-side-description"
          fullWidth
          maxWidth={maxWidth}
        >
          <DialogTitle
            disableTypography
            id="ebs-dialog-slide-title"
            className="m-0 p-2"
          >
            <Typography className="font-ebs text-xl">{title}</Typography>
            <IconButton
              aria-label="close"
              className="absolute right-px top-px fill-current text-gray-500"
              onClick={handleClose}
            >
              <Close />
            </IconButton>
          </DialogTitle>
          {children}
        </Dialog>
      </div>
    );
  }
);
// Type and required properties
EbsDialogAtom.propTypes = {
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  title: PropTypes.node,
  maxWidth: PropTypes.oneOf(["xs", "sm", "md", "lg", "xl", false]),
  children: PropTypes.node,
};
// Default properties
EbsDialogAtom.defaultProps = {
  open: false,
  maxWidth: "md",
};

export default EbsDialogAtom;
