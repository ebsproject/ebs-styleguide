import EbsDialog from "./EbsDialog";
import React from "react";
import { render, cleanup, screen } from "@testing-library/react";
import "@testing-library/dom";
import "@testing-library/jest-dom/extend-expect";

afterEach(cleanup);

test("EbsDialog is in the DOM", () => {
  render(<EbsDialog></EbsDialog>);
  expect(screen.getByTestId("EbsDialogTestId")).toBeInTheDocument();
});
