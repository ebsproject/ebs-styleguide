import React, { useState } from "react";
import PropTypes from "prop-types";
// CORE COMPONENTS
import SwipeableViews from "react-swipeable-views";
import { Tabs, Tab, Typography } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import TabPanel from "./TabPanel";

function a11yProps(index) {
  return {
    id: `tab-${index}`,
    "aria-controls": `tab-panel-${index}`,
  };
}

//MAIN FUNCTION
/*
 @param orientation: Tab orientation
 @param tabs: Tab components and labels
 @param ref: reference made by React.forward
*/
const EbsTabsLayoutAtom = React.forwardRef(({ orientation, tabs }, ref) => {
  const [value, setValue] = useState(0);
  const theme = useTheme();

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  return (
    /* 
     @prop data-testid: Id to use inside EbsTabsLayout.test.js file.
     */
    <div
      data-testid={"EbsTabsLayoutTestId"}
      ref={ref}
      className={`flex-grow bg-white ${
        (orientation === "vertical" && "flex h-full") || "w-full"
      }`}
    >
      <Tabs
        orientation={orientation}
        variant="scrollable"
        value={value}
        onChange={handleChange}
        aria-label="ebs-tab-atom"
        className="border-solid border-r-1 border-white"
        aria-label="ebs-tabs-layout"
      >
        {tabs.map((tab, index) => (
          <Tab
            key={index}
            label={<Typography className="text-ebs">{tab.label}</Typography>}
            {...a11yProps(index)}
          />
        ))}
      </Tabs>
      <SwipeableViews
        axis={theme.direction === "rtl" ? "x-reverse" : "x"}
        index={value}
        onChangeIndex={handleChangeIndex}
      >
        {tabs.map((tab, index) => (
          <TabPanel
            key={index}
            value={value}
            index={index}
            dir={theme.direction}
          >
            {tab.component}
          </TabPanel>
        ))}
      </SwipeableViews>
    </div>
  );
});
// Type and required properties
EbsTabsLayoutAtom.propTypes = {
  orientation: PropTypes.oneOf(["horizontal", "vertical"]),
  tabs: PropTypes.arrayOf(
    PropTypes.shape({ label: PropTypes.node, component: PropTypes.node })
  ).isRequired,
};
// Default properties
EbsTabsLayoutAtom.defaultProps = {
  orientation: "vertical",
};

export default EbsTabsLayoutAtom;
