import React from "react";
import PropTypes from "prop-types";

/*
@param children: Children component to display in tab panel
@param value: Current tab panel displayed
@param index: Unique index to display tab panel
@param ...other: rest of not functional properties
*/
export default function TabPanel({ children, value, index, ...other }) {
  return (
    <div
      data-testid="tabpaneltestid"
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      className="container w-full overflow-auto h-full"
      {...other}
    >
      {value === index && <div className="p-4">{children}</div>}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};
