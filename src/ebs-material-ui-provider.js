import * as materialUI from "@material-ui/core";
import * as materialUIStyles from "@material-ui/core/styles";
import * as materialUIUtils from "@material-ui/core/utils";
import * as muiStyles from "@material-ui/styles";
import * as muiIcons from "@material-ui/icons";
import * as muiColors from "@material-ui/core/colors";
import { create } from "jss";
import jssExtend from "jss-plugin-extend";
import muiTheme from "./mui-theme";

for (let key in materialUI) {
  materialUI[key].default = materialUI[key];
  System.set(System.resolve(`@material-ui/core/${key}`), materialUI[key]);
}

for (let key in muiIcons) {
  muiIcons[key].default = muiIcons[key];
  System.set(System.resolve(`@material-ui/icons/${key}`), muiIcons[key]);
}

System.set(System.resolve(`@material-ui/core`), materialUI);
System.set(System.resolve(`@material-ui/icons`), muiIcons);
System.set(System.resolve(`@material-ui/core/styles`), materialUIStyles);
System.set(System.resolve(`@material-ui/core/utils`), materialUIUtils);
System.set(System.resolve(`@material-ui/core/colors`), muiColors);
System.set(System.resolve(`@material-ui/styles`), muiStyles);

const jss = create({
  ...materialUIStyles.jssPreset(),
  plugins: [...materialUIStyles.jssPreset().plugins, jssExtend()],
  insertionPoint: document.getElementById("jss-insertion-point"),
});

const generateClassName = materialUIStyles.createGenerateClassName({
  seed: "ebs",
});

const theme = materialUIStyles.createTheme(muiTheme);

export function EBSMaterialUIProvider(props) {
  return (
    <materialUIStyles.StylesProvider
      jss={jss}
      generateClassName={generateClassName}
    >
      <materialUIStyles.MuiThemeProvider theme={theme}>
        {props.children}
      </materialUIStyles.MuiThemeProvider>
    </materialUIStyles.StylesProvider>
  );
}
