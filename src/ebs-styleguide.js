export { EBSMaterialUIProvider } from "./ebs-material-ui-provider";
export { default as EbsTabsLayout } from "catalog/EbsTabsLayout";
export { default as EbsDialog } from "catalog/EbsDialog";
export * as Core from "@material-ui/core";
export * as Icons from "@material-ui/icons";
export * as Styles from "@material-ui/core/styles";
export * as Colors from "@material-ui/core/colors";
