const path = require("path");
const { merge } = require("webpack-merge");
const singleSpaDefaults = require("webpack-config-single-spa-react");

module.exports = (webpackConfigEnv, argv) => {
  const defaultConfig = singleSpaDefaults({
    orgName: "ebs",
    projectName: "styleguide",
    webpackConfigEnv,
    argv,
  });

  const cssRule = defaultConfig.module.rules.find(
    (r) =>
      Array.isArray(r.use) && r.use.find((u) => u.loader.includes("css-loader"))
  );
  cssRule.use.push({
    loader: "postcss-loader",
  });

  return merge(defaultConfig, {
    // modify the webpack config however you'd like to by adding to this object
    resolve: {
      alias: {
        catalog: path.resolve(__dirname, "./src/catalog/"),
      },
    },
  });
};
